Mo 21 Nov 2022 19:48:19 CET

yamlMerger.pl dient der Fusion beliebig vieler YAML-Dateien der folgenden Struktur:

Beispiel Inhalt mehrerer YAML-Dateien mit jeweils einem zweistelligen Länderpräfix:

es.yml enthält Folgendes:

YAMLFREUNDE:   
  ES: amigos de YAML  

fr.yml enthält Folgendes:

YAMLFREUNDE:   
  FR: amis de YAML  

de.yaml enthält Folgendes

YAMLFREUNDE:  
  DE: YAML-Freunde  

en.yml enthält Folgendes:

YAMLFREUNDE:  
  EN: YAML friends  

Ergebnis nach "Fusion":

YAMLFREUNDE:
  DE: YAML-Freunde
  EN: YAML friends
  ES: amigos de YAML
  FR: amis de YAML

Der ursprüngliche Zweck: eine Menge Übersetzungsdateien in einer einzigen Datei
zu verschmelzen, um die Übersetzung an einem einzigen Ort zur Verfügung zu haben.
