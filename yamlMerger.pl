#!/usr/bin/env perl
#===============================================================================
#
#         FILE: yamlMerger.pl
#
#        USAGE: ./yamlMerger.pl  
#
#  DESCRIPTION: yamlMerger.pl dient dem Mergen mehrer ähnlich aufgebauter
#  				zweidimensionaler YAML-Dateien zwecks Erzeugung einer
#  				dreidimensionalen Zieldatei	(YAML). Das Skript benötigt keine
#  				Parameter und zieht alle YAML-Dateien des Formats ??.yml heran,
#  				aus denen es eine einzige Datei erstellt. Die Basis der Dateien
#  				(??) wird als Länderpräfix verwendet. Unter dem jeweiligen
#  				Schlüssel stehen in erster Ebene ebendiese Länderpräfixe als
#  				Unterschlüssel,	auf die nach dem Doppelpunkt die
#  				länderspezifischen Übersetzungswerte stehen.
#
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Andreas Grell
# ORGANIZATION: ---
#      VERSION: 1.1
#      CREATED: 21.11.2022 18:04:36
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Data::Dump qw( dump );
use File::Basename;
use File::Slurp;
use Log::Log4perl qw( :easy );
use YAML::XS qw( Dump DumpFile Load LoadFile );

unlink 'locales.yml';

my $logfile = basename( $0, '.pl' ) . '.log';

Log::Log4perl->easy_init( {
	level   => $DEBUG,
	file    => $logfile,
	utf8    => 1,
	layout => 'Datei %F{1} Zeile %L Zeit %d - %p - %m%n'
	} );

my $log = Log::Log4perl->get_logger( $0 );

$log->info( "BEGINN" );

my @dateien = glob( '??.yml' );

my $locales;

for my $datei ( @dateien ) {
	my $land = uc basename( $datei, ".yml" );
	my $yaml = LoadFile( $datei );
	for my $key ( sort { $a cmp $b } keys %$yaml ) {
		push @{ $locales->{ $key } }, { $land, $yaml->{ $key } };
	}	
}

DumpFile( 'locales.yml', $locales );

$locales = read_file( 'locales.yml' );

$locales =~ s/- /  /g;

unlink 'locales.yml';

open my $out, ">", 'locales.yml';

print $out $locales;

close $out;

$log->info( "ENDE -- WENN SIE DIESE ZEILE SEHEN, IST DAS SKRIPT ORDNUNGSGEMÄSS DURCHGELAUFEN." );
